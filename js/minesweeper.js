(function () {
    "use strict";

    var WIDTH = 32;
    var HEIGHT = 32;
    var FIELD_EXT = 20;
    var MINES = 150;

    var _clearedCount;
    var _correctFlagCount;
    var _playField = [];
    var _elemField;

    var SquareState = {
        Covered: 1, Flagged: 2, Cleared: 3, Mine: 4, FlaggedMine: 5, BlownUp: 6 
    };

	/**
	 * Class SquareInfo 
	 */
	var SquareInfo = (function() {
		
		/**
		 * Constructor 
		 */
		function SquareInfo(elem, textElem, state, adjMines, pos) {
			this.elem = elem;
			this.textElem = textElem;
			this.state = state;
			this.adjMines = adjMines;
			this.pos = pos;
		}				
		
		return SquareInfo;
	})();

    function getSquareInfo(x, y) {
        var pos = (y * WIDTH) + x;
        return _playField[pos];
    }

    function getNeighbours(pos) {
        var result = [];

        for (var j = -1; j < 2; ++j) {
            for (var i = -1; i < 2; ++i) {
                if ((i == 0) && (j == 0)) continue;
                if (((pos.x + i) < 0) || ((pos.x + i) >= WIDTH)) continue;
                if (((pos.y + j) < 0) || ((pos.y + j) >= HEIGHT)) continue;
                var neighbour = getSquareInfo(pos.x + i, pos.y + j);
                result.push(neighbour);
            }
        }

        return result;
    }

    function clearSquare(squareInf) {
        if (squareInf.state == SquareState.Covered) {
            squareInf.state = SquareState.Cleared;
            ++_clearedCount;
            squareInf.elem.classList.remove("covered");

            if (!squareInf.adjMines) {
                var neighbours = getNeighbours(squareInf.pos);
                neighbours.forEach(function (neighbour) {
                    if (neighbour.state == SquareState.Covered) {
                        clearSquare(neighbour);
                    }
                });
            }
            else {
                squareInf.textElem.textContent = squareInf.adjMines;
            }
        }
    }

    function drawOverlay(text) {
        var elemOvr = document.createElement("DIV");
        elemOvr.classList.add("overlay");
        elemOvr.style.marginLeft = _elemField.style.marginLeft;
        elemOvr.style.marginTop = _elemField.style.marginTop;
        elemOvr.style.width = _elemField.style.width;
        elemOvr.style.height = _elemField.style.height;

        var elemP = document.createElement("P");
        elemP.textContent = text;
        elemOvr.appendChild(elemP);

        document.body.appendChild(elemOvr);

        elemP.style.marginLeft = -elemP.clientWidth / 2 + "px";
        elemP.style.marginTop = -elemP.clientHeight / 2 + "px";
    }

    function blowUp(squareInf) {
    	squareInf.elem.classList.add("exploded");
    	squareInf.state = SquareState.BlownUp; 
        // drawOverlay("Bäm!");
        var x = squareInf.elem.offsetLeft, y = squareInf.elem.offsetTop;
        var elemAnim = document.createElement("DIV");
        elemAnim.style.left = x - 37 + "px";
        elemAnim.style.top = y - 40 + "px";
        elemAnim.classList.add("explosion");        
        _elemField.appendChild(elemAnim);        
    }

	function blowUpNext() {
		for (var j = 0; j < HEIGHT; ++j) {
			for (var i = 0; i < WIDTH; ++i) {
				var squareInf = getSquareInfo(i, j);
				if ((squareInf.state == SquareState.FlaggedMine) || (squareInf.state == SquareState.Mine)) {
					blowUp(squareInf);
					return;
				}
			}
		}		
	}

	function blowUpField() {
		setInterval(blowUpNext, 200);
	}

    function leftClickHandler(x, y) {
        var squareInf = getSquareInfo(x, y);

        switch (squareInf.state) {
            case SquareState.Covered:
                clearSquare(squareInf);
                break;

            case SquareState.Mine:                
                blowUp(squareInf);
                blowUpField();
                break;
        }
    }

    function rightClickHandler(x, y)  {
        var squareInf = getSquareInfo(x, y);
        if (squareInf.state == SquareState.Cleared) return;

        switch (squareInf.state) {
            case SquareState.Covered:
                squareInf.state = SquareState.Flagged;
                squareInf.elem.classList.remove("covered");
                squareInf.elem.classList.add("flagged");
                break;

            case SquareState.Mine:
                squareInf.state = SquareState.FlaggedMine;
                squareInf.elem.classList.remove("covered");
                squareInf.elem.classList.add("flagged");
                ++_correctFlagCount;
                break;

            case SquareState.Flagged:
                squareInf.state = SquareState.Covered;
                squareInf.elem.classList.add("covered");
                squareInf.elem.classList.remove("flagged");
                break;

            case SquareState.FlaggedMine:
                squareInf.state = SquareState.Mine;
                squareInf.elem.classList.add("covered");
                squareInf.elem.classList.remove("flagged");
                --_correctFlagCount;
                break;
        }
    }

    function win() {
        drawOverlay("You did it!");
    }

    function checkGameState() {
        if (_clearedCount + _correctFlagCount == WIDTH * HEIGHT) {
            win();
        }
    }

    function squareMouseUpHandler(e) {
        var coords = e.target.squareCoords;

        switch (e.which) {
            case 1: // Left
                leftClickHandler(coords.x, coords.y);
                break;

            case 3: // Right
                rightClickHandler(coords.x, coords.y);
                break;
        }

        checkGameState();
    }

    function createSquareElem(x, y) {
        var elemSquare = document.createElement("DIV");
        elemSquare.classList.add("square");
        elemSquare.classList.add("covered");
        elemSquare.style.width = FIELD_EXT - 1 + "px";
        elemSquare.style.height = FIELD_EXT - 1 + "px";
        elemSquare.style.left = x * FIELD_EXT - 1 + "px";
        elemSquare.style.top = y * FIELD_EXT - 1 + "px";
        elemSquare.squareCoords = { x: x, y: y };
        elemSquare.addEventListener("mouseup", squareMouseUpHandler);
        elemSquare.addEventListener("contextmenu", function (e) { e.preventDefault(); });

        var elemSquareText = document.createElement("P");
        elemSquareText.classList.add("squareText");
        elemSquare.appendChild(elemSquareText);
        _elemField.appendChild(elemSquare);
        
		_playField.push(new SquareInfo(elemSquare, elemSquareText, SquareState.Covered, 0, { x: x, y: y }));
    }

    function generateField() {
        _elemField = document.createElement("DIV");
        _elemField.id = "playField";
        _elemField.style.width = WIDTH * FIELD_EXT - 1 + "px";
        _elemField.style.height = WIDTH * FIELD_EXT - 1 + "px";

        for (var j = 0; j < HEIGHT; ++j) {
            for (var i = 0; i < WIDTH; ++i) {
                createSquareElem(i, j);
            }
        }

        document.body.appendChild(_elemField);

        var width = _elemField.clientWidth, height = _elemField.clientHeight;
        _elemField.style.marginLeft = -width / 2 + "px";
        _elemField.style.marginTop = -height / 2 + "px";
    }

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function addMines() {
        var count = 0;

        while (count < MINES) {
            var x = randomIntFromInterval(0, WIDTH - 1), y = randomIntFromInterval(0, HEIGHT - 1);
            var squareInf = getSquareInfo(x, y);
            if (squareInf.state != SquareState.Mine) {
                squareInf.state = SquareState.Mine;
                ++count;
                // squareInf.elem.style.backgroundColor = "#FF8844";
            }
        }
    }

    function calcVals() {
        for (var j = 0; j < HEIGHT; ++j) {
            for (var i = 0; i < WIDTH; ++i) {
                var squareInf = getSquareInfo(i, j);
                if (squareInf.state == SquareState.Mine) continue;
                var pos = { x: i, y: j };
                var neighbours = getNeighbours(pos);
                neighbours.forEach(function (neighbour) {
                    if (neighbour.state == SquareState.Mine) { ++squareInf.adjMines; }
                });

                //if (squareInf.adjMines) {
                //    squareInf.elem.querySelector("p").textContent = squareInf.adjMines;
                //}
            }
        }
    }

    function init() {
        _clearedCount = 0;
        _correctFlagCount = 0;
        generateField();
        addMines();
        calcVals();
    }

    function windowLoadHandler() {
        init();
    }

    window.addEventListener("load", windowLoadHandler);
})();
